<?php get_header(); ?>
<?php
	if( is_singular('members') ){
				get_template_part('templates/about-company');
				get_template_part('templates/blog-product');
                get_template_part('templates/contactmap');
                get_template_part('templates/contact');
            }
    elseif( is_singular('products') ){
    	get_template_part('templates/detail');
    }
?>
<?php get_footer(); ?>