<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="card" style="background: #FFFFFF">
            <?php if ( has_post_thumbnail() ) { ?>
                <div class="card-image">
                    <?php the_post_thumbnail( 'thumbnail' ); ?>
                    <?php } ?>
                </div>
            <div class="card-content">
                    <h4 class="card-title entry-title" style="margin: 0;">
                    <a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark" style="color: black">
                        <?php echo wp_kses( force_balance_tags( get_the_title() ), $allowed_html ); ?>
                    </a>
                    </h4>
                        <p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p>
            </div>
        </div>
</article>