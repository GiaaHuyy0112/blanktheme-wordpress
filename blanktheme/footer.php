<footer id="footer">

	<?php if( is_front_page() ){
			get_template_part('templates/contact' );
			} ?>

				<h2 id="footer-logo" ><?php get_logo(); ?></h2>
				<?php blanktheme_menu('footer-menu'); ?>
				<div id="social">
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<a href="#" class="fa fa-google"></a>
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<a href="#" class="fa fa-google"></a>
				</div>
                <div class="copyright">
                        <?php bloginfo('name'); ?>,<?php echo date('Y'); ?>,<a href="<?php get_url(); ?>privacy-policy">Term & Policy</a>,Legal Notice
                        <br>&copy;Copyright <?php echo date('Y'); ?>
                </div>
        </footer>
        </div> <!--end #container-body -->
        <?php wp_footer(); ?>

</body> <!--end body-->
</html> <!--end html -->