<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]> <html <?php language_attributes(); ?>> <![endif]-->
 
<head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <link rel="profile" href="http://gmgp.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Be+Vietnam:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,300;1,400;1,500;1,600;1,700;1,800&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Arimo:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

        <?php wp_head(); ?>
</head>
 
<body <?php body_class(); ?> > <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
        <div id="container container-body">
                <?php if( is_front_page() ){
                        ?>
                        <header id="header-home" <?php set_header_background();?>>
                        <?php
                        }elseif( is_page('ABOUT US')){
                        ?>
                        <header id="header-about" style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/07/shutterstock_640938316-1.png')                                   ;background-size: cover;
                                                                background-repeat: no-repeat;">
                        <?php
                        }elseif( is_singular('members') ){
                            ?>
                        <header id="header-members" 
                                style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/07/shutterstock_640938316-1-1.png')
                                        ;background-size: cover;background-repeat: no-repeat;">
                        <?php
                        }elseif( is_page('competences') ){ ?>
                            <header id="header-competences" 
                            style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/07/shutterstock_640938316-1-1-1.png')
                            ;background-size: cover;background-repeat: no-repeat">
                            <?php
                        }elseif( is_page('contact-us') ){
                            ?>
                                <header id="header-contact" 
                            style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/07/contact-bg.png')
                            ;background-size: contain;background-repeat: no-repeat; background-position: right;">
                            <?php
                        }elseif( is_page('our-team') ){
                            ?>
                            <header id="header-management" 
                            style="background:linear-gradient(180deg, #1A9DD7 0%, rgba(1, 168, 147, 0) 100%), url('<?php echo get_url(); ?>wp-content/uploads/2020/07/image-7.png')
                            ;background-size: cover; background-repeat: no-repeat;">


                            <?php
                        }elseif( is_page('privacy-policy') ){ ?>
                            <header id="header-policy" 
                            style="background:linear-gradient(180deg, #1A9DD7 0%, rgba(1, 168, 147, 0) 100%), url('<?php echo get_url(); ?>wp-content/uploads/2020/07/image-38.png')
                            ;background-size: cover;background-repeat: no-repeat;">
                            <?php
                        }elseif( is_page('products') || is_search() ){ ?>
                            <header id="header-product" 
                            style="background: linear-gradient(180deg, #01A893 0%, rgba(1, 168, 147, 0) 100%), url('<?php echo get_url(); ?>wp-content/uploads/2020/07/image-23.png')
                            ;background-size: cover;background-repeat: no-repeat;">
                            <?php
                        }elseif( is_singular('products') ){ ?>
                            <header id="header-product" 
                            style="background: linear-gradient(180deg, #01A893 0%, rgba(1, 168, 147, 0) 100%), url('<?php echo get_url(); ?>wp-content/uploads/2020/07/image-25.png')
                            ;background-size: cover;background-repeat: no-repeat;">
                            <?php
                        }?>
		<div class="logosite">
		    <h2 id="logo" ><?php get_logo(); ?></h2>
		      <?php blanktheme_menu('primary-menu'); ?>
                <!-- <a id="lan" href="#" style="margin: auto; text-decoration: none;"><img src="<?php get_url(); ?>wp-content/uploads/2020/07/Group.png"></a> -->
                        <nav id="mobile-menu" class="mobile-menu" >
                        <button style=" border: none;
                                        float: right;
                                        background: white;
                                        font-size: 20px;
                                        color: #01A893;"
                                onclick="myFunction()"
                                ><i class="fa fa-navicon"></i></button>
                                <?php blanktheme_menu('mobile-menu'); ?>
                        </nav>
			</div>
		    <?php get_home_header();?>
		</header>


<?php
// Tao header cho home
function get_home_header() {
  if( is_front_page() ){
  ?>
            <div class=header-home-container>
            <h1 id='home-title'>WE BOOST YOUR COMPANY!</h1>
            <p id='home-subtilte'>Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
            <a class='button' href="<?php get_url(); ?>contact-us/" >CONTACT US</a>
            </div>
  <?php
  }elseif(is_page('ABOUT US')){
    ?>
            <div class="header-about-container">
                <div class="title-container">
                  <div class="header-content content">
                    <h2 id="title" class="title-<?php echo get_slug(); ?>" > <?php echo get_the_title();?> </h1>
                    <p id="title-subtilte">Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
                  </div>
                  <div class="images">
                    <img src="<?php get_url(); ?>wp-content/uploads/2020/07/shutterstock_640938316-2-1.png">
                  </div>
                </div>
            </div>
    <?php
  }elseif( is_singular('members') ){
    ?>
     <div class="header-members" style="height: fit-content; padding-bottom: 50px;">
        <div class="header-content header-members-content">
            <h2 id="title" class="title-<?php echo get_slug(); ?>" >
                <?php echo get_the_title();?> </h1>

                <p id="title-subtilte" style="text-align: left; font-weight: bold"><?php echo get_field('location'); ?></p>
        </div>
    </div>
    <?php
  }elseif( is_page('competences') ){
    ?>
    <div class="header-competences" style="height: 300px">
        <div class="header-content header-competences-content">
            <h2 id="title" class="title-<?php echo get_slug(); ?>">
                <?php echo get_the_title();?> </h1>
            <p id="title-subtilte">Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>

        </div>
    </div>

    <?php
  }elseif( is_page('contact-us') ){
    ?>
        <?php get_template_part('templates/form'); ?>

    <?php
  }elseif( is_page('our-team') ){
    ?>
       <div class="header-management" style="height: 300px">
        <div class="header-content header-management-content">
            <h2 id="title" class="title-<?php echo get_slug(); ?>" style="margin-bottom: 20px;
                                                                                margin-top: 10px; color:white; " >
                <?php echo get_the_title();?> </h1>

                <p id="title-subtilte" style="color: white; width: 70%;">
                Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
        </div>
    </div>

    <?php
  }elseif( is_page('privacy-policy') ){
    ?>
    <div class="header-policy" style="height: 300px">
        <div class="header-content header-policy-content">
            <h2 id="title" class="title-<?php echo get_slug(); ?>">
                <?php echo get_the_title();?> </h1>
            <p id="title-subtilte">Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>

        </div>
    </div>

    <?php
  }elseif( is_page('products')){
    ?>
       <div class="header-products">
        <div class="header-content header-products-content">
                        <h2 id="title" class="title-<?php echo get_slug(); ?>" >
                        <?php echo get_the_title();?> </h2>
                <p id="title-subtilte" style="color: white; width: 100%;">
                Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
                <form method="get" action="<?php echo site_url(); ?>" style="display: flex; height: 50px;">
                    <input id="search-input" type="text" name="s" placeholder="Enter your keywords..." style="border-radius: 5px; border: none;">
                    <button type="submit" id="search-btn" class="button" >SEARCH</button>
                </form>
        </div>
        <?php 
            if( is_page('products')){
                ?>
                    <div class="header-image">
                         <img style=" position: relative; top: 30%; width: 100%;" src="<?php get_url(); ?>wp-content/uploads/2020/07/Image-Thumb.png">
                    </div>
                <?php
            } ?>
        
    </div>

    <?php
  }elseif( is_search() ){
    ?>
       <div class="header-search">
        <div class="header-content header-search-content">
            <h2 id="title" class="title-<?php echo get_slug(); ?>" style="font-size: 50px; margin-bottom: 20px;
                                                                            margin-top: 10px; color:white; ">
            <?php echo "All results for ".$search_keyword;?> </h2>
                <p id="title-subtilte" style="color: white; width: 100%;">
                Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
                <form method="get" action="<?php echo site_url(); ?>" style="display: flex; height: 50px;">
                    <input type="text" name="s" placeholder="Enter your keywords..." style="border-radius: 5px; border: none; width: 250px;">
                    <button type="submit" id="search-btn" class="button" >SEARCH</button>
                </form>
        </div>
    </div>

    <?php
  }elseif(is_singular('products')){
    ?>
            <div class="header-detail-container">
                <div class="images-product">
                    <?php the_post_thumbnail('medium_large'); ?>
                  </div>
                <div class="title-container">
                  <div class="header-content content">
                    <h2 id="title" class="title-<?php echo get_slug(); ?>" style="margin: 0 ;font-size: 36px;" > <?php echo get_the_title();?> </h1>
                        <?php 
                            if( have_posts() ){
                                the_post();
                                the_content();
                                if( have_rows('table_info') ){
                                    $i = 1;
                                    ?>
                                    <table id="prod-info">
                                    <?php
                                    while ( have_rows('table_info')) {
                                        the_row();
                                        $name = get_sub_field('name');
                                        $des = get_sub_field('des');
                                        ?>
                                            <tr>
                                                <td><ul><li><?php echo $name; ?></li></ul></td>
                                                <td><?php echo $des; ?></td>
                                            </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?> 
                                    </table>
                                    <?php
                                }
                            }
                        ?>
                  </div>
                </div>
            </div>
    <?php
  } //endif
}
?>