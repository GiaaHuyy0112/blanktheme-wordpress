function createLocation(la,ln){

	var pos = { lat: la, lng: ln };
	// var location = new google.maps.Marker(document.getElementById("map"), {
	// 	center: { lat: N, lng: E },
	// 	zoom: 8,
	// 	title: title
	// });

	// var location = new google.maps.Marker( { position: post , map: map  });
	return pos;
}

if(document.querySelector('#map') != null)
{
	var script = document.createElement('script');
	script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSZ61ZVLHi6jNC-C-BEiEnGev6sxu2G28&callback=initMap';
	script.defer = true;
	script.async = true;
	document.head.appendChild(script);
}
	var arrayMarker = [];
	var arrayInfo = [];
	var arrayLocation = [];
	const buttons = document.querySelectorAll('.grid-button .grid-item-button .card-button');
	const gridBtn = document.querySelector('.grid-button');
	var auto = "auto ";
	if( width >= 1200 && document.URL.includes("contact-us")){
		gridBtn.style.gridTemplateColumns = auto.repeat(4);
	}

	var map;

function initMap() {
 if( document.URL.includes('contact-us') ){
 		map = new google.maps.Map(document.getElementById("map"), {
    	zoom: 17
  		});
	if( document.URL.includes("contact-us") ){
		if( document.querySelector('.map-template') != null ){
			// console.log(buttons.length);
			for( i = 0; i < buttons.length; i++)
			{
				const v = buttons[i].value;
				const arr = v.split(",");
				const title = buttons[i].querySelector('h3').textContent;
				var la = parseFloat( arr[0]);
				var ln = parseFloat( arr[1]);
				// createLocation(N,E);
				// arrayLocation.push( createLocation(N,E) );
				//console.log( title );
				var pos = createLocation(la, ln);
				var marker = new google.maps.Marker({ position: { lat: la, lng: ln }, map: map });
				var info = new google.maps.InfoWindow({ content: '<h1>' + title + '</h1>' });

				arrayLocation.push( pos );
				arrayMarker.push(marker);
				arrayInfo.push(info);
			}
		}
	}
	 arrayInfo[ arrayInfo.length-1 ].open( map, arrayMarker[ arrayMarker.length-1 ] );
	 map.setCenter(arrayLocation[ arrayLocation.length-1 ]);
 }
 else{
 	map = new google.maps.Map(document.getElementById("map"),{
 		zoom: 17
 	});
 	var arr = document.querySelector('#position').textContent.split(",");
 	var pos = createLocation( parseFloat(arr[0]), parseFloat(arr[1]) );
 	// console.log(pos);
 	var marker = new google.maps.Marker( { position: pos, map: map } );
 	map.setCenter(pos);
 }
 


}
function clickMap(i){
 	// console.log(i);
 	const btn = document.querySelectorAll('.grid-infomation');
 	// console.log(document.documentElement.clientWidth);
 	for (var j = 0; j < buttons.length ; j++) {
 		if(j != i){
 			arrayInfo[j].close();
 			if( document.documentElement.clientWidth < 1000 )
 			{
 				btn[i].style.display = "grid";
 				btn[j].style.display = "none";
 			}
 		}
 		
 	};
 	arrayInfo[i].open( map, arrayMarker[i] );
 	map.setCenter(arrayLocation[i]);
 	document.querySelector('.map-template').scrollIntoView();
 }
