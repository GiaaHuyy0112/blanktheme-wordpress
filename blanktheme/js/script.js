function myFunction(){
	var x = document.querySelector("#mobile-menu .menu ul");
	var menu = document.querySelector("#mobile-menu")
	if ( x.style.display === "grid" ){
		x.style.display = "none";
		menu.style.background = "transparent";
	}else{
		x.style.display = "grid";
		menu.style.background = "white";
	}
}


if ( document.URL.includes("about-us") || document.URL.includes("product") ){
	if( document.querySelector('.blog-members .grid-container') != null ){
		const gridblog = document.querySelector('.blog-members .grid-container');
		const griditems = document.querySelectorAll('.blog-members .grid-container .grid-item');
		var width = document.documentElement.clientWidth;
	    var height = document.documentElement.clientHeight;
	    var i = Math.floor((Math.random() * (griditems.length -1 )) + 1);
		    if(width <= 1000 && width > 700){
		    	var item = griditems[i].querySelector('.card');
		    	gridblog.style.gridTemplateColumns = "auto auto";
		    	griditems[i].style.gridArea = "1 / 1 / 1 / 2";
		    	// item.style.width = "100%";
		    	//console.log(i);
		    }
		    else if(width <= 1200 && width > 1000){
		    	var item = griditems[i].querySelector('.card');
		    	gridblog.style.gridTemplateColumns = "auto auto auto";
		    	griditems[i].style.gridArea = "1 / 1 / 1 / 4";
		    	item.style.width = "99%";
		    	//console.log(i);
		    }
		    else if(width > 1200){
		    	if( (griditems.length/2) != 1  && griditems.length > 3)
				{
				// console.log(griditems.length);
				// for ( i = 0; i < griditems.length ; i++) {
				// 	const img = griditems[i].querySelector(".card .card-image img");
				// 	// console.log(img.height);
				// 	if(img.height > 200){
				var item = griditems[i].querySelector('.card');
				gridblog.style.gridTemplateColumns = "auto auto auto auto";
					griditems[i].style.gridRowStart = "2";
					griditems[i].style.gridRowEnd = "2";
					griditems[i].style.gridColumnStart = "3";
					griditems[i].style.gridColumnEnd = "5";

					item.style.width = "100%";
				// 	}
				// }

				}
		    	//console.log(i);
		    }
		    else if (width < 700){
		    	// console.log(griditems.length, i);
		    	gridblog.style.gridTemplateColumns = "auto";
		    	griditems[i].style.gridArea = "1 / 1 / 1 / 1";
		    }
	}


}


if (document.URL.includes("contact-us")){
	var width = document.documentElement.clientWidth;
	if( width < 1200){
		var header = document.querySelector('#header-contact');
		var grid = document.querySelector('.grid-contactform');
			header.style.backgroundSize = "60% 100%";
			grid.style.width = "fit-content"
		}
	if( width < 1000){
		var header = document.querySelector('#header-contact');
			header.style.background = "";
		}
		// console.log(width);
}




function grid(i){
		const gridblog = document.querySelector('.blog-members #grid-members');
		const griditems = document.querySelectorAll('.blog-members #grid-members .grid-item');
	 	//console.log(document.documentElement.clientWidth);
	    // Lấy thông số
	    var width = document.documentElement.clientWidth;
	    var height = document.documentElement.clientHeight;
	    if(width <= 1200 && width > 700){
	    	gridblog.style.gridTemplateColumns = "auto auto";
	    	griditems[i].style.gridArea = "3 / 1 / 5 / 1";
	    	//console.log(i);
	    }
	    else if(width > 1200){
	    	gridblog.style.gridTemplateColumns = "auto auto auto auto";
	    	griditems[i].style.gridArea = "1 / 4 / 3 / 5";
	    	//console.log(i);
	    }
	    else if (width < 700){
	    	gridblog.style.gridTemplateColumns = "auto";
	    	griditems[i].style.gridArea = "1 / 1 / 1 / 1";
	    }
	     
	};

if ( document.URL.includes("") ){
	if( document.querySelector('.blog-members #grid-members') != null ) {
		const gridblog = document.querySelector('.blog-members #grid-members');
		const griditems = document.querySelectorAll('.blog-members #grid-members .grid-item');
		if( (griditems.length/2) != 1  && griditems.length > 3)
		{
			// console.log(griditems.length);
			var i;
			for ( i = 0; i < griditems.length ; i++) {
				const img = griditems[i].querySelector(".card .card-image img");
				// console.log(img.height);
				if(img.height > 200){
					gridblog.style.gridTemplateColumns = "auto auto auto auto";
		    		griditems[i].style.gridArea = "1 / 4 / 3 / 5";
					break;
				}
			}

		}
		if(document.documentElement.clientWidth < 1200){
			window.onresize = grid(i);
		}
	}

// const cards = document.querySelectorAll('.blog-members #grid-members .grid-item');
}



if( document.querySelector('#carousel') != null ){

	const slide = document.querySelector('.carousel-slide');
	const images = document.querySelectorAll('.carousel-slide .card');

	const pB = document.querySelector('#prevBtn');
	const nB = document.querySelector('#nextBtn');

	let c = 0;
	var stop = 0;
	const size = images[0].clientWidth;

	// console.log(-(((size + 10)*images.length) - slide.clientWidth));



	slide.style.transform = 'translateX(' + (-size * c) + 'px)';


	nB.addEventListener('click', () =>{
		if( c >= images.length - 1 ) return;
		if( images.length * size < slide.clientWidth ) return;
		else if( ( (images.length - c)-1 )* size < slide.clientWidth ){
			slide.style.transition="transform 0.4s ease-in-out";
			slide.style.transform = 'translateX(' + (-(((size + 10)*images.length) - slide.clientWidth)) + 'px)';
			if(stop == 0){
				c++;
				stop = 1;
			}
			return
		}

		slide.style.transition="transform 0.4s ease-in-out";
		c++;
		slide.style.transform = 'translateX(' + (-size * c) + 'px)';
	});

	pB.addEventListener('click', () =>{
		if( c <= 0 ) return;
		slide.style.transition="transform 0.4s ease-in-out";
		stop = 0;
		c--;
		slide.style.transform = 'translateX(' + (-size * c) + 'px)';
	});

	slide.addEventListener('transitionend', () =>{
		if(c == images.length ){
			slide.style.transition = "none";
			c = 0;
			slide.style.transform = 'translateX(' + (-size * c) + 'px)';
		}
		if(c < 0){
			slide.style.transition = "none";
			c = 0;
			slide.style.transform = 'translateX(' + (-size * c) + 'px)';
		}
	});
}

function clickManager(i){
	const table = document.querySelector('.grid-manager-'+ i);
	
	const text = document.querySelector('.dropdown-head-content-' + i);

	const head = document.querySelector('.dropdown-head-' + i + ' button');
	// head.style.display = "none";
	if(table.style.display == "none"){
		text.style.display = "inline";
		table.style.display = "inline";
		head.innerHTML = 'Close <i class="fa fa-chevron-up" style="font-size: 15px;"></i>';
	}else{
		// console.log(table.style.display)
		text.style.display = "none";
		table.style.display = "none";
		head.innerHTML = 'View all Managers <i class="fa fa-chevron-down" style="font-size: 15px;"></i>';
	}
	
}
function clickProducts(i){

	const dropdown = document.querySelector('.dropdown-' + i);

	const head = document.querySelector('.dropdown-head-' + i);

	const btn = document.querySelector('.dropdown-head-' + i + ' .btn-product');

	const title = document.querySelector('.dropdown-head-' + i + ' .title');

	const titledrop = document.querySelector('.dropdown-head-' + i + ' .dropdown-title');

	const catBtn = document.querySelector('.dropdown-' + i + ' .category-btn');

	// head.style.display = "none";
	if(dropdown.style.display == "none"){
		catBtn.click();
		dropdown.style.display = "block";
		btn.innerHTML = 'Close <i class="fa fa-chevron-up" style="font-size: 15px;"></i>';
		titledrop.style.display = "block";
		head.style.background = "#01A893";
		btn.style.color = "#01A893";
		btn.style.background = "white";
		title.style.display = "none";
	}else{
		// console.log(table.style.display)
		dropdown.style.display = "none";
		btn.innerHTML = 'View all Products <i class="fa fa-chevron-down" style="font-size: 15px;"></i>';
		title.style.display = "block";
		titledrop.style.display = "none";
		head.style.background = "#F1F1F1";
		btn.style.background = "#01A893";
		btn.style.color = "white";
	}
	
}




const item1 = document.querySelector('.menu-item-has-children');
item1.addEventListener("mouseover", function(){
	var submenu = document.querySelector('.sub-menu');
	submenu.style.display = "grid";
	item1.addEventListener("mouseout", function(){
		submenu.style.display = "none";
	});

	// console.log(1);
	const item2 = document.querySelector('.sub-menu .menu-item-has-children');
	var submenu2 = document.querySelector('.sub-menu .menu-item-has-children .sub-menu');
	item2.addEventListener("mouseover", function(){
		submenu2.style.display = "grid";
	});
	item2.addEventListener("mouseout", function(){
		submenu2.style.display = "none";
	});
});






function clickInfo(i)
{
	// console.log(i);
	const content = document.querySelector('.info-content-' + i);
	const button = document.querySelector('.info-table .button-' + i);
	const title = document.querySelector('.info-table .table-title-' + i);

	if (content.style.display == "none")
	{
		content.style.display = "block";
		title.style.fontWeight= "bold";
		title.style.color = "#01A893";
		button.style.background = "#01A893";
		button.className = 'fa fa-angle-up button-' + i;
	}
	else{
		content.style.display = "none";
		title.style.fontWeight= "normal";
		title.style.color = "#061032";
		button.style.background = "#F0F2F4";
		button.className = 'fa fa-angle-down button-' + i;
	}
}

function clickPopup(i){
	// popup(1);
	const popup = document.querySelector('.popup-' + i);

	const background = document.querySelector('.background-' + i);
	const body = document.querySelector('body').clientHeight;

	popup.style.display = "block";
	popup.scrollIntoView({block: "center"});
	background.style.display = "block";
	background.style.height = body + 'px';

	const btn = document.querySelector('.close-btn-'+ i);
	btn.onclick = function(){
		popup.style.display = "none";
		background.style.display = "none";
	};

	background.onclick = function(){
		popup.style.display = "none";
		background.style.display = "none";
	};

	return false;
};




if( document.querySelector('#timeline') != null ){

	const slide = document.querySelector('.timeline-slide');
	const timeline = document.querySelectorAll('#timeline .grid-timeline .container-timeline');

	const pB = document.querySelector('#prevTimeBtn');
	const nB = document.querySelector('#nextTimeBtn');

	let c = 0;
	var stop = 0;
	const size = timeline[0].clientWidth;

	slide.style.transform = 'translateX(' + (-size * c) + 'px)';

	if( timeline.length > 1){
		var i = 0;
		var color = 0;
		for( i = 0; i < timeline.length; i++ ){
			const content = timeline[i].querySelector('.container-timeline .content-box');
			const title = timeline[i].querySelector('.container-timeline .content-date');
			if( color == 0 )
			{
				content.style.background = "#F9C478";
				// console.log("document.location : "+ document.location.pathname);
				var str = title.style.background.toString();
				var str2 = str.substring(0,str.lastIndexOf("/")+1 );
				str2 = str2.concat("wp-content/uploads/2020/07/1.png");
				// console.log( str2);
				title.style.background = str2;
				title.style.backgroundRepeat = "no-repeat";
				title.style.backgroundSize = "contain";
			}
			else if( color == 1 )
			{
				content.style.background = "#01A893";
				var str = title.style.background.toString();
				var str2 = str.substring(0,str.lastIndexOf("/")+1 );
				str2 = str2.concat("wp-content/uploads/2020/07/2-1.png");
				// console.log( str2);
				title.style.background = str2;
				title.style.backgroundRepeat = "no-repeat";
				title.style.backgroundSize = "contain";
			}
			else if( color == 2 )
			{
				content.style.background = "#1A9DD7";
				var str = title.style.background.toString();
				var str2 = str.substring(0,str.lastIndexOf("/")+1 );
				str2 = str2.concat("wp-content/uploads/2020/07/3-1.png");
				// console.log( str2);
				title.style.background = str2;
				title.style.backgroundRepeat = "no-repeat";
				title.style.backgroundSize = "contain";
			}
			else
			{
				content.style.background = "#D65C52";
				var str = title.style.background.toString();
				var str2 = str.substring(0,str.lastIndexOf("/")+1 );
				str2 = str2.concat("wp-content/uploads/2020/07/4-1.png");
				// console.log( str2);
				title.style.background = str2;
				title.style.backgroundRepeat = "no-repeat";
				title.style.backgroundSize = "contain";
				color = -1;
			}
			color++;
		}

		if( timeline.length >= 4){
			const gridTimeline = document.querySelector('#timeline .grid-timeline');
			gridTimeline.style.gridTemplateColumns = 'repeat(' + timeline.length + ', 250px)';
			gridTimeline.style.width = (250 * timeline.length ) + 'px';

		}
	}





	nB.addEventListener('click', () =>{
		if( c >= timeline.length - 1 ) return;
		if( timeline.length * size < slide.clientWidth ) return;
		else if( ( (timeline.length - c)-1 )* size < slide.clientWidth ){
			slide.style.transition="transform 0.4s ease-in-out";
			slide.style.transform = 'translateX(' + (-((size*timeline.length) - slide.clientWidth)) + 'px)';
			if(stop == 0){
				c++;
				stop = 1;
			}
			return
		}
		slide.style.transition="transform 0.4s ease-in-out";
		c++;
		slide.style.transform = 'translateX(' + (-size * c) + 'px)';
	});

	pB.addEventListener('click', () =>{
		if( c <= 0 ) return;
		slide.style.transition="transform 0.4s ease-in-out";
		stop = 0;
		c--;
		slide.style.transform = 'translateX(' + (-size * c) + 'px)';
	});

	slide.addEventListener('transitionend', () =>{
		if(c == timeline.length ){
			slide.style.transition = "none";
			c = 0;
			slide.style.transform = 'translateX(' + (-size * c) + 'px)';
		}
		if(c < 0){
			slide.style.transition = "none";
			c = 0;
			slide.style.transform = 'translateX(' + (-size * c) + 'px)';
		}
	});
}




function getClass(n,id){
	const show = document.querySelectorAll('.dropdown-table-' + n + ' .product-'+ id);
	const hidden = document.querySelectorAll('.dropdown-table-' + n + ' .products');
	const currentBtn = document.querySelector('.dropdown-table-' + n + ' .category-btn-'+id);
	const listBtn = document.querySelectorAll('.dropdown-table-' + n + ' .category-btn');
	for (var i = 0 ; i < listBtn.length ; i++) {
		listBtn[i].className = listBtn[i].className.replace(" btn-active", "");
	}
	currentBtn.className += " btn-active";

	for (var i = 0 ; i < hidden.length ; i++) {
		hidden[i].style.display = "none";
	}

	for (var i = 0 ; i < show.length ; i++) {
		show[i].style.display = "block";
	}


}

if( document.URL.includes("?s"))
{
	const search = document.querySelector('.search-grid');
	// console.log(document.documentElement.clientWidth);
	if( document.documentElement.clientWidth < 700 ){
		// console.log(document.documentElement.clientWidth);
		search.style.gridTemplateColumns = "auto";

	}else if( document.documentElement.clientWidth < 1000 ){
		// console.log(document.documentElement.clientWidth);
		search.style.gridTemplateColumns = 'repeat(2, auto)';

	}else if( document.documentElement.clientWidth < 1200 ){
		// console.log(document.documentElement.clientWidth);
		search.style.gridTemplateColumns = 'repeat(3, auto)';

	}else{
		// console.log(search);
		search.style.gridTemplateColumns = 'repeat(4, auto)';
	}



	const hidden = document.querySelectorAll('.search-results .products');
	const btn = document.querySelector('#loadmore-btn');
	const title = document.querySelector('#title');
	const keyword = document.querySelector('#keyword').textContent;
	title.innerHTML = "All results for '" + keyword + "'";
	for (var i = 2; i< hidden.length; i++) {
		hidden[i].style.display = "none";
		hidden[i].className += " product-none";
	}
	if( hidden.length <= 2 )
	{
		btn.style.display = "none";
	}
}


function loadmore(){
	const hiddens = document.querySelectorAll('.search-results .product-none');
	const search = document.querySelector('.search-info strong');
	var value = parseInt(search.textContent);
	// console.log(value);

	for(i = 0; i< hiddens.length; i++){
		if(i == 5){
			break;
		}
		hiddens[i].style.display = "block";
		hiddens[i].className = hiddens[i].className.replace(" product-none", "");
		search.innerHTML = parseInt(value) + parseInt(i+1);

	}
	if(hiddens.length == 0){
		const btn = document.querySelector('#loadmore-btn');
		btn.style.display = "none";
	}

}

if( document.querySelector('#menu-mobile') != null){
	const item = document.querySelector('#menu-mobile .menu-item-has-children');
	const submenu = document.querySelector('#menu-mobile .menu-item-has-children .sub-menu');
	item.addEventListener('click', function(){
		if ( submenu.style.display != "block"){
			item.style.display = "block";
			submenu.style.display = "block";
			submenu.style.position = "relative";
		}
		else{
			item.style.display = "flex";
			submenu.style.display = "none";
		}
	});
}


if( document.querySelector('.menu') != null ){
	const primary = document.querySelector('.primary-menu .language');
	const mobile = document.querySelector('.mobile-menu .language');
	primary.innerHTML = '<a href="#" ><img src="http://training.azpassio.com/auxilto/wp-content/uploads/2020/07/Group.png"></a>';
	mobile.innerHTML = '<a href="#" ><img src="http://training.azpassio.com/auxilto/wp-content/uploads/2020/07/Group.png"></a>';

}