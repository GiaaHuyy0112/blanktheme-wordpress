<?php get_header(); ?>
 
<div id="content" style="margin-top: 100px;">
    <section id="search-content" style="margin-bottom: 50px;">
        <div class="search-info">
                <!--Sử dụng query để hiển thị số kết quả tìm kiếm được tìm thấy
                        Cũng như hiển thị từ khóa tìm kiếm. Từ khóa tìm kiếm cũng
                        có thể hiển thị được với hàm get_search_query()-->
                <?php
                        $search_query = new WP_Query( array( 'post_type' => 'products',
                                                                'posts_per_page' => -1,
                                                                's' => $s ) );
                        $search_keyword = wp_specialchars( $s, 1);
                        $search_count = $search_query->post_count;
                        //var_dump( $search_query );
                        if ($search_count > 0)
                        {
                            printf( __('Showing 1-<strong>2</strong> of <strong>%2$s</strong>  results for "<strong id="keyword">%1$s</strong>"', 'blanktheme'), $search_keyword, $search_count );
                        }
                        else{
                            printf( __('There is no any results for "<strong id="keyword">%1$s</strong>"', 'blanktheme'), $search_keyword);
                        }
                ?>
        </div>
                <div class="search-grid" style="display: grid; width: fit-content; margin: auto; grid-gap: 25px;">
                        <?php if ( $search_query->have_posts() ) : while ( $search_query->have_posts() ) : $search_query->the_post(); ?>
                                <?php get_template_part( 'content', get_post_format() ); ?>
                        <?php endwhile; ?>
                        <?php blanktheme_pagination(); ?>
                        <?php else : ?>
                                <?php get_template_part( 'content', 'none' ); ?>
                        <?php endif; ?>
                </div>
                <button id="loadmore-btn" class="button button-green" onclick="loadmore()" style="margin-top: 50px;">LOAD MORE PRODUCTS</button>
        </section>
    <?php get_template_part('templates/contact'); ?>
</div>
 
<?php get_footer(); ?>