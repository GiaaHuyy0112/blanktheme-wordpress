<?php
/**
  @ Thiết lập các hằng dữ liệu quan trọng
  @ THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
  @ CORE = thư mục /core của theme, chứa các file nguồn quan trọng.
  **/
  define( 'THEME_URL', get_stylesheet_directory() );
  define( 'CORE', THEME_URL . '/core' );

  /**
  @ Load file /core/init.php
  @ Đây là file cấu hình ban đầu của theme mà sẽ không nên được thay đổi sau này.
  **/
 
  require_once( CORE . '/init.php' );

  /**
 @ Thiết lập $content_width để khai báo kích thước chiều rộng của nội dung
 **/
 if ( ! isset( $content_width ) ) {
       /*
        * Nếu biến $content_width chưa có dữ liệu thì gán giá trị cho nó
        */
       $content_width = 1920;
  }

  /**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
  **/
  if ( ! function_exists( 'blanktheme_setup' ) ) {
        /*
         * Nếu chưa có hàm thachpham_theme_setup() thì sẽ tạo mới hàm đó
         */
        function blanktheme_setup() {

        /*
        * Tự chèn RSS Feed links trong <head>
        */
        add_theme_support( 'automatic-feed-links' );


        /*
        * Thêm chức năng post thumbnail
        */
        add_theme_support( 'post-thumbnails' );

        /*
        * Thêm chức năng title-tag để tự thêm <title>
        */
        add_theme_support( 'title-tag' );

        /*
        * Thêm chức năng post format
        */
        add_theme_support( 'post-formats',
                            array(
                               'image',
                               'video',
                               'gallery',
                               'quote',
                               'link'
                            )
                         );

        /*
        * Thêm chức năng custom background
        */
        $default_background = array(
        'default-color' => '#FFFFFF',
        );
        add_theme_support( 'custom-background', $default_background );


        /*
        * Tạo menu cho theme
        */
        register_nav_menus( array(
        'primary-menu' => 'Primary Menu',
        'footer-menu' => 'Footer Menu',
        'mobile-menu' => 'Mobile Menu'
         ) );
        add_theme_support( 'menus' );



        /*
        * Tạo sidebar cho theme
        */
        $sidebar = array(
        'name' => __('Main Sidebar', 'blanktheme'),
        'id' => 'main-sidebar',
        'description' => 'Main sidebar for Blank theme',
        'class' => 'main-sidebar',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
        );
        register_sidebar( $sidebar );
 
        }


        // Custom Logo
        add_theme_support( 'custom-logo', array(
            'height'      => 100,
            'width'       => 400,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => array( 'site-title', 'site-description' ),
        ) );


        $defaults = array(
        'default-image'          => '',
        'random-default'         => false,
        'width'                  => 0,
        'height'                 => 0,
        'flex-height'            => false,
        'flex-width'             => false,
        'default-text-color'     => '',
        'header-text'            => true,
        'uploads'                => true,
        'wp-head-callback'       => '',
        'admin-head-callback'    => '',
        'admin-preview-callback' => '',
        'video'                  => false,
        'video-active-callback'  => 'is_front_page',
        );
        add_theme_support( 'custom-header', $defaults );


        // Block style
        add_theme_support( 'wp-block-styles' );


        // Wide Alignment
        add_theme_support( 'align-wide' );


        // Editor Style
        add_theme_support( 'editor-styles' );
 
  }
  add_action('after_setup_theme','blanktheme_setup');

/**
@ Thiết lập hàm hiển thị logo
**/
  if ( ! function_exists( 'get_logo' ) ) {
  function get_logo() {
    ?>
      <div class="logo">
   
        <div class="site-name">
          <?php if ( is_home() ) {
            if(get_custom_logo() != '')
              the_custom_logo();
            else{
              bloginfo('name');
            }
          } else {
            if(get_custom_logo() != '')
              the_custom_logo();
            else{
              bloginfo('name');
            }
          } // endif ?>
        </div>
        <div class="site-description"><?php bloginfo( 'description' ); ?></div>
   
      </div>
    <?php 
  }
}

/**
@ Thiết lập hàm hiển thị menu
@ thachpham_menu( $slug )
**/
if ( ! function_exists( 'blanktheme_menu' ) ) {
  function blanktheme_menu( $slug ) {
    $menu = array(
      'theme_location' => $slug,
      'container' => 'nav',
      'container_class' => 'menu '.$slug,
    );
    wp_nav_menu( $menu );
  }
}

/**
@ Chèn CSS và Javascript vào theme
@ sử dụng hook wp_enqueue_scripts() để hiển thị nó ra ngoài front-end
**/
function blanktheme_style() {
  /*
   * Hàm get_stylesheet_uri() sẽ trả về giá trị dẫn đến file style.css của theme
   * Nếu sử dụng child theme, thì file style.css này vẫn load ra từ theme mẹ
   */
  wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', 'all' );
  wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js', array ( 'jquery' ), 1.1, true);
  // wp_enqueue_script('custom', get_stylesheet_directory_uri().'/scripts/timeline.js');
  wp_enqueue_style( 'main-style' );
}
add_action( 'wp_enqueue_scripts', 'blanktheme_style' );
add_action('acf/input/admin_enqueue_scripts', 'blanktheme_style');




// Set header background
function set_header_background(){
  if(get_header_image() != '' && is_front_page()){
    printf('style="background-image:url(%1$s); background-repeat: no-repeat; background-size: cover"',get_header_image());
  }
}




/**
@ Tạo hàm phân trang cho index, archive.
@ Hàm này sẽ hiển thị liên kết phân trang theo dạng chữ: Newer Posts & Older Posts
@ thachpham_pagination()
**/
if ( ! function_exists( 'blanktheme_pagination' ) ) {
  function blanktheme_pagination() {
    /*
     * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
     */
    if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
      return '';
    }
  ?>
 
  <nav class="pagination" role="navigation">
    <?php if ( get_next_post_link() ) : ?>
      <div class="prev"><?php next_posts_link( __('Older Posts', 'blanktheme'), 'products' ); ?></div>
    <?php endif; ?>
 
    <?php if ( get_previous_post_link() ) : ?>
      <div class="next"><?php previous_posts_link( __('Newer Posts', 'blanktheme'), 'products' ); ?></div>
    <?php endif; ?>
 
  </nav><?php
  }
}



/**
@ Hàm hiển thị ảnh thumbnail của post.
@ Ảnh thumbnail sẽ không được hiển thị trong trang single
@ Nhưng sẽ hiển thị trong single nếu post đó có format là Image
@ thachpham_thumbnail( $size )
**/
if ( ! function_exists( 'blanktheme_thumbnail' ) ) {
  function blanktheme_thumbnail( $size ) {
 
    // Chỉ hiển thumbnail với post không có mật khẩu
    if ( ! is_single() &&  has_post_thumbnail()  && ! post_password_required() || has_post_format( 'image' ) ) : ?>
      <figure class="post-thumbnail"><?php the_post_thumbnail( $size ); ?></figure><?php
    endif;
  }
}



/**
@ Hàm hiển thị tiêu đề của post trong .entry-header
@ Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
@ Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
@ thachpham_entry_header()
**/
if ( ! function_exists( 'blanktheme_entry_header' ) ) {
  function blanktheme_entry_header() {
    if ( is_single() ) : ?>
 
      <h1 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h1>
    <?php else : ?>
      <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
          <?php the_title(); ?>
        </a>
      </h2><?php
 
    endif;
  }
}


/*
 * Thêm chữ Read More vào excerpt
 */
function blanktheme_readmore( ) {
  return '...<a class="read-more" onclick="return clickPopup('.get_the_ID().')" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'blanktheme') . '</a>';
}
add_filter( 'excerpt_more', 'blanktheme_readmore' );

/**
@ Hàm hiển thị nội dung của post type
@ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
@ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
@ thachpham_entry_content()
**/
if ( ! function_exists( 'blanktheme_entry_content' ) ) {
  function blanktheme_entry_content() {
 
      the_content();
 
  }
}


/**
@ Hàm hiển thị tag của post
@ thachpham_entry_tag()
**/
if ( ! function_exists( 'blanktheme_entry_tag' ) ) {
  function blanktheme_entry_tag() {
    if ( has_tag() ) :
      echo '<div class="entry-tag">';
      printf( __('Tagged in %1$s', 'blanktheme'), get_the_tag_list( '', ', ' ) );
      echo '</div>';
    endif;
  }
}



// Advanced Custom Fields Plugin
if( function_exists('acf_add_options_page') ) {
acf_add_options_page( array(
'page_title' => 'Theme options', // Title hiển thị khi truy cập vào Options page
'menu_title' => 'Theme options', // Tên menu hiển thị ở khu vực admin
'menu_slug' => 'theme-settings', // Url hiển thị trên đường dẫn của options page
'capability' => 'edit_posts',
'redirect' => false
));
}

if( function_exists('get_category_post') ){
  get_category_post();
}


  /**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    if( is_page('products') || is_singular('products') || is_search() )
      return 10;
    elseif( is_front_page() )
      return 5;
    else
      return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );




//Lay URL cua page
function get_url() {
  $url = home_url('/');
  echo $url;
}

//GET Slug page
function get_slug(){
  global $post;
  $post_slug = $post->post_name;
  echo $post_slug;
}




function my_acf_init() {
  
  acf_update_setting('google_api_key', 'AIzaSyCSZ61ZVLHi6jNC-C-BEiEnGev6sxu2G28');
}

add_action('acf/init', 'my_acf_init');