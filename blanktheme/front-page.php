<?php get_header(); ?>


<section id="main-content">
        <?php get_template_part('templates/about' ); ?>
        <?php get_template_part('templates/blog' ); ?>
        <?php get_template_part('templates/partners' ); ?>
</section>

<?php get_footer(); ?>