<?php 
function blog($title, $subtitle, $catname, $excerpt, $type){
		$cat = array(
				'post_type' => $type,
				'category_name' => $catname
		);
		$loop = new WP_Query($cat);

		$allowed_html = array(
			'br'     => array(),
			'em'     => array(),
			'strong' => array(),
			'i'      => array(
				'class' => array(),
			),
			'span'   => array(),
		);
		?>
		<div class="blog blog-<?php echo $catname ?> blog-page-<?php echo get_slug(); ?>">
			<div class="blog-content">
				<h2 id="blog-title"><?php echo $title ?></h2>
				<p id="blog-subtitle"><?php echo $subtitle ?></p>
			</div>
		<div id="grid-<?php echo $catname ?>" class="grid-container">
		<?php
		$counter = 1;
		while ( $loop->have_posts() ){
			$loop->the_post();
		?>
				<article class="grid-item grid-member-<?php echo $counter ?>">
					<div class="card card-<?php echo $catname ?>">
						<?php if ( has_post_thumbnail() ) { ?>
								<div class="card-image">
									<?php the_post_thumbnail( '' ); ?>
									<?php } ?>
								</div>
								<div class="card-content">
									<h4 class="card-title entry-title" style="margin: 0;">
									<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark">
										<?php echo wp_kses( force_balance_tags( get_the_title() ), $allowed_html ); ?>
									</a>
									</h4>
									<?php if ($excerpt == true) {
										?>
										<!-- <p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p> -->
										<?php the_content(); ?>
										<?php
									}else{
										?>
										<p><?php the_content(); ?></p>
										<?php
									} ?>
							</div>
							<a class="read-more" href="<?php echo get_permalink(); ?>">Read More</a>
					</div>	
				</article>

		<?php
		$counter++;
		}
		wp_reset_postdata();
		?>
					</div>
		</div>
<?php
	}
?>

<?php
function get_post_noimg($title, $subtitle,$catname){
		$cat = array(
				'post_type' => 'members',
				'category_name' => $catname,
		);
		$loop = new WP_Query($cat);
		?>
		<div class="blog blog-<?php echo $catname ?> blog-page-<?php echo get_slug(); ?>">
			<div class="blog-content">
				<h2 id="blog-title"><?php echo $title ?></h2>
				<p id="blog-subtitle"><?php echo $subtitle ?></p>
			</div>
		<div class="grid-container">
		<?php
		while ( $loop->have_posts() ){
			$loop->the_post();
		?>
				<article class="grid-item">
					<div class="card card-<?php echo $catname ?>">
								<div class="card-content">
									<h4 class="card-title entry-title" style="margin: 0;">
									<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark">
										<?php echo get_the_title(); ?>
								</a>
							</h4>
						</div>
					</div>	
				</article>

		<?php
		}
		wp_reset_postdata();
		?>
					</div>
		</div>
<?php
	}
?>



			<?php if(is_front_page() ) { ?>
				<div class="blog-template" style="background: #FFFFFF">
				<?php blog('OUR MEMBER COMPANIES',
					'The Auxilto Group of Companies are supported by a global network of successful entrepreneurs particularly in the Healthcare industry',
					'members',
					'true', 
					'members'

				); ?>
				<?php get_template_part('templates/blog-product');?>
				</div>
			<?php } else{
				?>
				<div class="blog-template" style="background: #F3F8F7">
				<?php
				get_post_noimg('OUR MEMBER COMPANIES',
					'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget fermentum leo. Donec vehicula ornare nibh non porttitor. Ut non pharetra sem. Vivamus ac justo nunc. Fusce feugiat, nisl vitae imperdiet maximus, libero risus ullamcorper diam, vitae laoreet dolor magna a eros.'
					,'members');
				?>
				</div>
				<?php
			} ?>
