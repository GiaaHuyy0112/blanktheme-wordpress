<div class="policy-template">
	<div class="policy-title">
		<h3 class="text-type1">PRIVACY POLICY</h3>
		<h2 class="text-strong">The <strong>AUXILTO Policy</strong></h2>
	</div>
	<div class="policy-content" style="margin-top: 100px">
		<?php
			if( have_rows('policy') ){
				while ( have_rows('policy') ) {
					the_row();
					$image = get_sub_field('image');
					?>
						<div class="container">
							<img src="<?php echo $image['url']; ?>">
							<div class="content">
								<h3 class="text-type1" style="text-transform: uppercase; font-size: 24px;"><?php echo get_sub_field('title'); ?></h3>
								<p class="text-paragraph">
									<?php echo get_sub_field('content'); ?>
								</p>
							</div>
						</div>
					<?php
				}
			}
		?>
	</div>
</div>