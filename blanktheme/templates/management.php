<?php 
    function popup($num, $term){
        ?>
            <div class="background-popup background-<?php echo $num ?>" style="background: rgba(0, 0, 0, 0.7);width: 100%;height: 100%;content: '';position: absolute;
    top: 0;left: 0;z-index: 1;display: none;"></div>
            <div class="popup popup-<?php echo $num ?>" style="height: fit-content; background: white; display: none; position: absolute; z-index: 1; text-align: center; border-radius: 10px;">
                <div class="popup-header">
                    <div class="images" style="height: 200px">
                        <?php the_post_thumbnail('medium'); ?>
                    </div>
                    <div class="info">
                        <h1 style="font-size: 55px; color: #01A893; margin: 0; line-height: 60px;"><?php the_title(); ?></h1>
                        <h3 style="color: #D65C52; margin: 0"><?php echo $term->name; ?></h3>
                    </div>
                </div>
                <div class="popup-content">
                    <?php the_content(); ?>
                </div>
                <a id="close-btn" class="button close-btn-<?php echo $num ?>">CLOSE</a>
            </div>
        
<?php
    }
?>

<div class="management-template">
<?php
$custom_terms = get_terms('company');
$num = 1;
foreach($custom_terms as $custom_term) {
    wp_reset_query();
    $args = array('post_type' => 'management',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'company',
                'field' => 'slug',
                'terms' => $custom_term->slug,
            ),
        ),
     );

     $loop = new WP_Query($args);
     if($loop->have_posts()) {?>
     	<div class="dropdown-table dropdown-table-<?php echo $num ?>" style="background: #F1F1F1; margin: auto;margin-bottom: 50px; border-radius: 16px;">
     	<div class="dropdown-head dropdown-head-<?php echo $num ?>" style="display: flex;background: #F1F1F1;border-radius: 10px; width: 95%; margin: auto;">
        	<h3 style="margin-left: 10px;color: #D65C52; width: 100%; font-size: 20px;"><?php echo $custom_term->name ?></h3>
        	<button onclick="clickManager(<?php echo $num; ?>)" style="align-self: center;" >
        			View all Managers <i class="fa fa-chevron-down" style="font-size: 15px;"></i></button>
        </div>
        <div class="dropdown-head-content dropdown-head-content-<?php echo $num ?>" style="display: none; padding: 20px;">
        	<h3 style="color: #D65C52; text-align: center; margin: 5px;">MANAGER OF</h3>
        	<h2 style="text-transform: uppercase; margin-top: 0;"><?php echo $custom_term->name ?></h2>
    	</div>

        <?php
        $counter = 1;
        ?> <div class="grid-manager grid-manager-<?php echo $num ?>" style="grid-template-columns: auto; padding: 50px; display: none" > <?php
        $n = 1;
        while($loop->have_posts()) : $loop->the_post();
            // echo '<a href="'.get_permalink().'">'.get_the_title().'</a><br>';
            ?>
            	<div class="grid-item" style=" margin: auto; text-align: left;" >
            		<?php if( $counter == 1 ){ ?>
            			<figure class="post-image">
	            			<?php the_post_thumbnail( 'medium' ); ?>
	            		</figure>
	            		<div class="grid-item-content" style="align-self: center; padding: 20px;">
	            			<h2 style="font-size: 36px; text-align: left;"><?php the_title(); ?></h2>
                            <h3 style="color: #D65C52" ><?php 
                                echo get_field("position");
                            ?></h3>
                            <?php popup(get_the_ID(),$term); ?>
	            			<p class="description-<?php echo $n ?>" ><?php echo get_the_excerpt(); ?></p>
	            		</div>
	            		<?php $counter++; }else{ ?>
			            			<div class="grid-item-content" style="align-self: center; padding: 20px;">
				            			<h2 style="font-size: 36px; text-align: left;"><?php the_title(); ?></h2>
                                        <h3 style="color: #D65C52" ><?php 
                                            echo get_field("position");
                                        ?></h3>
                                        <?php popup(get_the_ID(), $term); ?>
				            			<p class="description-<?php echo $n ?>"><?php echo get_the_excerpt(); ?></p>
			            			</div>
			            			<figure class="post-image">
	            						<?php the_post_thumbnail( 'medium' ); ?>
	            					</figure>

            		<?php
            		$counter = 1;
            		 } ?>
            	</div>

            <?php
            $n++;
        endwhile;
        ?> </div> </div> <?php
     }
     $num++;
}

 ?>
</div>






