<div class="grid-container grid-contactform">
	<div id="contactinfo" >
			<h3 style="text-align: left; margin-top: 5px; margin-bottom: 15px; font-weight: 800; font-size: 48px;">CONTACT US</h3>
			<p>Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
			<div class="grid-container" style="grid-template-columns: auto auto">
				<div class="grid-item grid-container" style="grid-template-columns: 30% 70%; text-align: left; width: 90%; margin-left: 0">

					<p class="info "><i class="material-icons">pin_drop</i> Address</p>
					<p>4228 Homestead Rd undefined Cedar Hill ,California 38597 United States</p>

					<p class="info"><i class="material-icons">email</i> Email</p>
					<p>michael.mitchell@example.com</p>

					<p class="info"><i class="material-icons">vibration</i> Phone</p>
					<p>(629) 555-0129</p>
			</div>
		</div>
	</div>
	<div class="contactform">
		<?php echo do_shortcode( '[wpforms id="940"]'); ?>
	</div>
</div>