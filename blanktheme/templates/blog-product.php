<?php 
function blogProduct($title, $subtitle, $excerpt, $readmore){
		$cat = array(
				'post_type' => 'post',
				'category_name' => 'product',
		);
		$loop = new WP_Query($cat);
		?>
		<div class="blog blog-product blog-page-<?php echo get_slug(); ?>">
			<div class="blog-content" style="margin-bottom: 30px;">
				<h2 id="blog-title"><?php echo $title ?></h2>
				<p id="blog-subtitle" style="margin: auto;"><?php echo $subtitle ?></p>
			</div>
			<div id="grid-product" class="grid-container">
			<?php
			while ( $loop->have_posts() ){
				$loop->the_post();
			?>
					<article class="grid-item">
						<div class="card card-product">
							<?php if ( has_post_thumbnail() ) { ?>
									<div class="card-image">
										<?php the_post_thumbnail( '' ); ?>
										<?php } ?>
									</div>
									<div class="card-content">
										<h4 class="card-title entry-title" style="margin: 0;">
										<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark">
											<?php the_title(); ?>
										</a>
										</h4>
										<?php if ($excerpt == 'true') {
											?>
											<?php the_excerpt(); ?>
											<?php
										}else{
											?>
											<p><?php the_content(); ?></p>
											<?php
										} ?>
								</div>
								<?php if( $readmore == 'true') {?>
								
							<?php }?>
						</div>	
					</article>

			<?php
			}
			wp_reset_postdata();
			?>
			</div>
		</div>
<?php } ?>

<?php if( is_front_page() ){
	blogProduct('OUR BUSSINESS FIELD',
					'Auxilto provides a platform with long-standing connections into all industries – from pharmaceuticals to cosmetics, fashion and lifestyle products. We are a global network of successful entrepreneurs, investing our long-term experience, market know-how and creative power into your business success.',
					'true',
					'true');
}elseif( is_singular('members') ){
	?>
	<div class="blog blog-product blog-page-<?php echo get_slug(); ?>">
			<div class="blog-content" style="margin-bottom: 30px;">
				<h2 id="blog-title">OUR BUSSINESS FIELD</h2>
				<p id="blog-subtitle" style="margin: auto;">Auxilto provides a platform with long-standing connections into all industries – from pharmaceuticals to cosmetics, fashion and lifestyle products. We are a global network of successful entrepreneurs, investing our long-term experience, market know-how and creative power into your business success.</p>
			</div>
			<div id="grid-product" class="grid-container">
	<?php
	$choices = get_field('products');
	if( $choices ){
		foreach ($choices as $choice) {
			$slug = $choice;
			$args = array(
				'name' => $slug,
				'post_type' => 'post'
			);
			$posts = get_posts($args);
			if( $posts ){
				$postID = $posts[0]->ID;
				$cat = array(
					'post_type' => 'post',
					'category_name' => 'product',
				);
				$loop = new WP_Query($cat);
				while ( $loop->have_posts() ){
					$loop->the_post();
					if( $postID == get_the_ID() ){
						?>
							<article class="grid-item">
								<div class="card card-product">
									<?php if ( has_post_thumbnail() ) { ?>
											<div class="card-image">
												<?php the_post_thumbnail( '' ); ?>
												<?php } ?>
											</div>
											<div class="card-content">
												<h4 class="card-title entry-title" style="margin: 0;">
												<a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark">
													<?php the_title(); ?>
												</a>
												</h4>
												<?php if ($excerpt == 'true') {
													?>
													<?php the_excerpt(); ?>
													<?php
												}else{
													?>
													<p><?php the_content(); ?></p>
													<?php
												} ?>
										</div>
										<?php if( $readmore == 'true') {?>
										
									<?php }?>
								</div>	
							</article>
						<?php
					}
				}
				wp_reset_postdata();
			}
		}
	}
	?>
		</div>
	</div>
	<?php

} ?>