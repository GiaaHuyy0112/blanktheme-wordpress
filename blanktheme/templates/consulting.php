<div class="consulting" style="background: url('<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-6.png ');
								background-size: cover;
								background-repeat: no-repeat;">
	<h3 style="color: white; text-align: center;">CONSULTING</h3>
	<div class="consulting-content" style="margin: auto; padding: 20px;">
	<p style="text-align: center; color: white">
		Do you want to optimize structures within your company?<br><br>

We advise, guide and share our long-standing experience.<br><br>

We analyze your current business situation, optimize procedures and help you with operational business as well as new strategic alignments. Through our global network, we open up new markets for your company. Our holistic solutions are practical and effective.<br><br>

<strong>It is our goal to accompany you on the way to new success.</strong>
	</p>
	</div>
</div>