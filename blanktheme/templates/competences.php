<div id="cometences-template" class="grid-competences">


	<div class="grid-container grid-competence">
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group.png">
			<div class="content">
				<h2>HEALTHCARE</h2>
				<p>Health is one of the most important values of our time and a synonym for a good life. As a central life goal, this value has been deeply embedded in our consciousness, our cultures and in every social class of society.<br><br>

Through self-acquired knowledge, people place greater expectations in companies, they challenge the healthcare system on a daily basis. Healthcare is an economic growth factor with permanent power of innovation. <br><br>

Therefore we want to manage your business holistically and offer you a global network, a broad portfolio ranging from state-of-the-art OTC medicines to low-cost generics with over 30 years of experience.
				</p>
			</div> <!-- ----end content---- -->
		</div> <!-- ----end item----- -->





	<div class="grid-container grid-competence">
			<div class="content">
				<h2>Food Supplements</h2>
				<p>Health is one of the most important values of our time and a synonym for a good life. As a central life goal, this value has been deeply embedded in our consciousness, our cultures and in every social class of society.<br><br>

Through self-acquired knowledge, people place greater expectations in companies, they challenge the healthcare system on a daily basis. Healthcare is an economic growth factor with permanent power of innovation. <br><br>

Therefore we want to manage your business holistically and offer you a global network, a broad portfolio ranging from state-of-the-art OTC medicines to low-cost generics with over 30 years of experience.
				</p>
			</div> <!-- ----end content---- -->
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-2.png">
	</div> <!-- ----end item----- -->




	<div class="grid-container grid-competence">
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-1.png">
			<div class="content">
				<h2>CANNABIS</h2>
				<p><strong>The cannabis market is one of the fastest growing markets.</strong><br><br>

The non-psychoactive ingredient cannabidiol (CDB) has many positive pharmacological proprties like regenerative, anxioytics, antidepressant, neuroprotective andsleep -promoting effects. CBD was released by the World Anti-Doping Agency for athletic competition<br><br>
In a joint venture with one of the largest European CBD producers, (European certified seeds, prodution compliant with GMP conditions) we focus our development primarily on external application in form of sports care products and cosmetics lines.
				</p>
			</div> <!-- ----end content---- -->
	</div> <!-- ----end item----- -->




	<div class="grid-container grid-competence">
			<div class="content">
				<h2>COSMETIX</h2>
				<p>We offer further development opportunities for your company and give you the possibility to be a pioneer with innovative and sustainable cosmetic products.<br><br>

The market of aesthetic medicia  as well as the cosmetics industry itself is boomiin, in particular natural cosmetics are given a new status. People are committed to sustainability these days.<br><br>

This worldwide interest is given to actively increase the well -being of mankind with helping to create high-quality products.<br><br>

Having a fresh natural and youthful look is what most people want.<br><br>
<strong>Feeling comfortable in your skin should be possible at any age.</strong>
				</p>
			</div> <!-- ----end content---- -->
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-3.png">
	</div> <!-- ----end item----- -->





	<div class="grid-container grid-competence">
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-4.png">
			<div class="content">
				<h2>M & A - Mergers and Acquisition</h2>
				<p><strong>Are you considering to sell your company or would you like to increase it through targeted acquisitions?</strong><br><br>

We accompany you with objectives, to the strategy, the target identification and the negotiation phase, to the conclusion of the contract and the implementation.<br><br>

Whether you are a large company or medium-sized enterprise, we support you with our expertise and experience in this organized process. Together we create a new way for your business!
				</p>
			</div> <!-- ----end content---- -->
		</div> <!-- ----end item----- -->





	<div class="grid-container grid-competence">
			<div class="content">
				<h2>INVESTMENT</h2>
				<p>We pursue a holistic, solution-oriented investment approach – from the development and implementation of a strategy through management to the exit of an asset.<br><br>

Within these processes we always follow a defined structure.<br><br>

<strong>We strive for continuous economic optimization and create an added value for your company with our expertise, an international network and decades of experience.</strong>
				</p>
			</div> <!-- ----end content---- -->
			<img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-5.png">
		</div> <!-- ----end item----- -->



</div> <!-- ----end container------- -->