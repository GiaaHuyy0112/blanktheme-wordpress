<?php
function contact($content, $btn)
{
	if( is_front_page() ){
      ?>
     <div class="email-signup" style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/06/Mask-Group.png');
     									background-repeat: no-repeat;
     									background-size: cover;">
      <div class="form grid-container">
        <p class="grid-item1">Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
        <form class="grid-item2">
          <input type="email" placeholder="Enter your email to start">
          <button type="submit">SIGN UP</button>
        </form>
      </div>
      </div>
    <?php
    } else{
        if( is_page('our-team') ){
          ?>
          <div class="image-contact">
          <img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-1-1.png"
              style="object-fit: contain; max-width: 100%;">
          </div>
          <?php
        }
      ?>
        <div class="email-signup" style="background: url('<?php echo get_url(); ?>wp-content/uploads/2020/06/Mask-Group.png');
     									background-repeat: no-repeat;
     									background-size: cover;">
          <p><?php echo $content ?></p>
          <a href="our-team" class="button button-white" style="text-decoration: none;"><?php echo $btn ?></a>
        </div>
      <?php
    }
}

if( is_page('about-us') || is_singular('members')  ){
	contact( 'We have a talented team responsible for developing our services and eusuring client satisfaction',
				'SEE OUR TEAM'
			);
}else{
	contact( 'Auxilto provides a platform with long-standing connections into all industries – from pharmaceuticals to cosmetics, fashion and lifestyle products. We are a global network of successful entrepreneurs, investing our long-',
				'GET STARTED'
			);
}
; 


?>
