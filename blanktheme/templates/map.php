<?php
	function createBtn($title, $num){
		?>
		<div class="grid-item grid-item-button">
			<button onclick="clickMap(<?php echo $num ?>)"
					class="card card-button card-button-<?php echo $num ?>" value="<?php echo get_field("la") ?>,<?php echo get_field("ln") ?>" >
				<div style="display: flex;">
					<i class="fa fa-map-marker" style="font-size: 20px; align-self: center;"></i>
					<h3><?php echo $title ?></h3>
				</div>
				<div class="grid-infomation">
					<p class="info-title">Address</p>
					<p><?php echo get_field("address"); ?></p>
					<p class="info-title">Email</p>
					<p><?php echo get_field("email"); ?></p>
					<p class="info-title">Phone</p>
					<p><?php echo get_field("phone"); ?></p>
				</div>
			</button>
		</div>
		<?php
	}

?>



<div class="map-template" style="background: #F0F2F4; padding-bottom: 50px;">
	<h2>OUR LOCATION</h2>
<div id="map" style="height: 500px; width: 80%; margin: auto;"></div>
<div class="grid-container grid-button" style="width: fit-content; margin: auto; grid-gap: 10px; padding-top: 10px;">
	<?php
	$button = array('post_type' => 'members');
	$loop = new WP_Query($button);
	$num = 0;
	while( $loop->have_posts() ){
		$loop->the_post();
		//$current = $loop->the_post();
		// $content = get_field( "e" );
			createBtn( get_the_title(), $num);
			$num++;
	}
	wp_reset_postdata();
	?>
</div>
</div>