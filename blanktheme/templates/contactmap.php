<?php 
?>
	<div style="background: #F3F8F7; background-size: cover;">
		<div id="contact-map" style="margin: auto; padding: 20px;">
			<h5 style="text-align: left;">GET IN TOUCH</h5>
			<h2 style="text-align: left; margin-top: 5px; margin-bottom: 15px;">CONTACT US</h2>
			<p style="width: 60%;">Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?</p>
			<div class="grid-container">
				<div class="grid-item grid-container" style="grid-template-columns: auto auto; text-align: left; width: 90%; margin-left: 0">

					<p class="info "><i class="material-icons">pin_drop</i>Address</p>
					<p><?php echo get_field('address'); ?></p>

					<p class="info"><i class="material-icons">vibration</i> Phone</p>
					<p><?php echo get_field('phone'); ?></p>

					<p class="info"><i class="fa fa-fax"></i> Fax</p>
					<p><?php echo get_field('fax'); ?></p>

					<p class="info"><i class="material-icons">email</i> Email</p>
					<p><?php echo get_field('email'); ?></p>

					<p class="info"><i class="material-icons">person</i> Contact</p>
					<p><?php echo get_field('contact'); ?></p>

					<p class="info"><i class="material-icons">public</i> Website</p>
					<p><?php echo get_field('website'); ?></p>

				</div>
				<div class="grid-item">
					<p id="position" style="display: none"><?php echo get_field('la')?>,<?php echo get_field('ln'); ?></p>
					<div id="map" style="height: 350px; width: 350px; margin: auto;"></div>
				</div>

			</div>
		</div>
	</div>

<?php
	 ?>