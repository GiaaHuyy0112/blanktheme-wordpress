<div class="product-detail" style="background: #F6F6F6;">
	<div class="product-infomation" style="width: 80%; margin: auto;">
		<h3 style="color: #D65C52; margin: 0; padding-top: 100px; font-size: 36px;">PRESCRIBING INFOMATION</h3>
		<?php 
		$num = 1;
		if( have_rows('infomation') ){
			while ( have_rows('infomation') ) {
				the_row();
				$name = get_sub_field('name');
				$des = get_sub_field('description');
				?>
					<div class="detail-table">
				<?php
						?>
						<div style="background: white; border-radius: 10px;">
							<div class="info-table" style="display: flex; background: white; margin: 10px;">
								<p class="table-title-<?php echo $num ?>" style="width: 100%; font-size: 24px; text-transform: capitalize;"><?php echo $name; ?></p>
								<button class="fa fa-angle-down button-<?php echo $num ?>" onclick="clickInfo(<?php echo $num?>)"
								 style="background: #F0F2F4;border-radius: 100px; font-size: 20px; color: #51545E; width: 50px; height: fit-content; margin: auto;
								 border: 0;"></button>
							</div>
							<div class="info-content info-content-<?php echo $num ?>" style="display: none;border-top: 1px solid #E7E8E9;padding: 15px;">
								<p><?php echo $des; ?></p>
							</div>
						</div>
						<?php
						$num++;
				?>

			</div>

				<?php
			}
		}
		?>
	</div>
	<div class="related" style="width: 80%; margin: auto;">
	<div class="related-title">
	<h3 style="color: #D65C52; font-size: 36px;">RELATED PRODUCTS</h3>
	<div id="carousel" class="carousel-container" style="height: max-content;">
		<div class="carousel-slide">
			<?php
				$posts = array(
					'post_type' => 'products',
					'posts_per_page' => 5
				);
				$loop = new WP_Query( $posts );
				if ( $loop -> have_posts() ) {
				    while ( $loop -> have_posts() ) {
				    	$loop->the_post();
				    	?>
				    	<div class="card card-related">
					    	<div class="card-image">
					    		<?php the_post_thumbnail( '' ); ?>
					    		</div>
					    	<div class="card-content" style="padding: 0;">
					    		<div style="padding: 10px;">
					    			<h4><?php the_title(); ?></h4>
					    			<p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p>
					    		</div>
				    	</div>
				    	</div>
				    	<?php
				    }
				}
				wp_reset_query();
			 ?>
		</div>
	</div>
	<div id="carousel-btn">
			<button id="prevBtn"> < </button>
			<button id="nextBtn"> > </button>
	</div>
</div>

</div>