<div class="client">
	<div class="client-title">
	<h2>CLIENTS SAY</h2>
	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget fermentum leo. Donec vehicula ornare nibh non porttitor. Ut non pharetra sem. Vivamus ac justo nunc. Fusce feugiat, nisl vitae imperdiet maximus, libero risus ullamcorper diam, vitae laoreet dolor magna a eros.</p>
	</div>
	<div id="carousel" class="carousel-container">
		<div class="carousel-slide">
			<?php
				$posts = array(
					'post_type' => 'client'
				);
				$loop = new WP_Query( $posts );
				if ( $loop -> have_posts() ) {
				    while ( $loop -> have_posts() ) {
				    	$loop->the_post();
				    	?>
				    	<div class="card card-client">
				    	<div class="card-image">
				    		<?php the_post_thumbnail( '' ); ?>
				    		</div>
				    	<div class="card-content">
				    		<div class="client-name">
								<p class="name"><?php the_title(); ?></p>
								<p><?php echo get_field('position'); ?></p>
							</div>
							<?php the_content(); ?>
				    	</div>
				    	</div>
				    	<?php
				    }
				}
				wp_reset_query();
			 ?>
		</div>
	</div>
	<div id="carousel-btn">
			<button id="prevBtn" class="fa fa-angle-left"></button>
			<button id="nextBtn" class="fa fa-angle-right"></button>
	</div>
</div>











