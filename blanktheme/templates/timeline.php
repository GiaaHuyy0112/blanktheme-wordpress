<?php

function top($year, $content){
  ?>
  <div class="timeline timeline-top">
    <div class="container-timeline container-timeline-top">
      <div class="content-date" style="background: url(<?php echo get_url(); ?>);">
        <h2><?php echo $year; ?></h2>
      </div>
      <div class="meta-date"></div>
      <div class="content-box">
        <?php echo $content; ?>
      </div>
    </div>
  </div>

  <?php
}

function bot($year, $content){
  ?>
  <div class="timeline timeline-bot">
    <div class="container-timeline container-timeline-bot">
      <div class="meta-date"></div>
      <div class="content-box">
        <?php echo $content; ?>
      </div>
      <div class="content-date" style="background: url(<?php echo get_url(); ?>);">
        <h2><?php echo $year; ?></h2>
      </div>
    </div>
  </div>

  <?php
}








 ?>



<div class="timeline-template" style="margin-bottom: 20px;">
  <h3 style="color: #D65C52; text-align: center; font-size: 25px;">ABOUT US</h3>
  <p style=" width: 80%; padding: 15px; margin: auto; text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis maximus lacus, tincidunt hendrerit orci egestas ac. Fusce malesuada hendrerit laoreet. Praesent blandit sem ut nunc congue suscipit.</p>
  <section id="timeline" class="main-timeline-section">
            <div class="conference-center-line"></div>
            <button id="nextTimeBtn" class="fa fa-angle-right"></button>
            <button id="prevTimeBtn" class="fa fa-angle-left"></button>
            <div class="timeline-cover" style="width: 65%; overflow: hidden; margin: auto;">
              <div class="timeline-slide">
                <div class="grid-timeline">

                  <?php
                    $timeline = array(
                              'post_type' => 'timeline',
                              'order' => 'ASC'
                    );
                    $loop = new WP_Query($timeline);
                    $counter = 1;
                    while ( $loop->have_posts() ){
                      $loop->the_post();
                      $content = get_the_content();
                      $title = get_the_title();


                      if($counter == 1){
                        top($title, $content );
                        $counter++;
                      }
                      else{
                        bot($title, $content);
                        $counter = 1;
                      }
                    }
                    wp_reset_postdata();
                  


                  ?>
                </div>
            </div>
          </div>
          </section>
        </div>