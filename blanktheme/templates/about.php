<?php if( is_front_page() ){
	?>
	<div id="home-about">
		<div id="about-us">
			<h3>ABOUT US</h3>
			<h2>The <strong style="color:#01A893;">AUXILTO GROUP</strong></h2>
			<p>The Auxilto Group of Companies operates in many countries across the globe. <br><br>

			With our connection and know how, the Auxilto Group supports all forms of activities relating to the Healthcare industries, ranging from pharmaceutical technologies, product development, licensing, manufacturing, trading of API and pharmaceutical products, immunological treatment for cancers, merger and acquisition, consultancy services and investment projects etc.<br><br></p>

			<p style="color:#01A893; font-weight:bold;">Boost your company with Auxilto!</p>

			<div id="about-info" class="grid-container">
				<div class="grid-item">
					<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/1.png">
					<h5>One Global Network</h5>
					<p>The Auxilto Group of Companies are supported by a global network of successful entrepreneurs particularly in the Healthcare industry</p>
				</div>
				<div class="grid-item">
					<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/2.png">
					<h5>Diversity</h5>
					<p>We respect and value the unique strengths and cultural differences of our associates, customers and community</p>
				</div>
				<div class="grid-item">
					<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/3.png">
					<h5>Constant improvement</h5>
					<p>We strive for continuous economic optimization and create an added value for your company with our expertise and international network</p>
				</div>
			</div>
		</div>
	</div>
	<?php
}else if( is_page('about-us') ){
	?>
	<div id="about-page" class="entry-content">
		<div class="wp-block-columns about-us">
			<div class="wp-block-column" style="flex-basis:30%">
			<h3 style="color:#d65c52; font-size: 20px" class="has-text-color"><strong>ABOUT US</strong></h3>
			<h2 class="has-very-dark-gray-color has-text-color"><strong>The <span style="color:#01a893" class="has-inline-color">AUXILTO</span></strong></h2>
			</div>
			<div class="wp-block-column" style="flex-basis:70%; margin: 0">
				<div class="wp-block-group"><div class="wp-block-group__inner-container">
				<p>The Auxilto Group of Companies operates in many countries across the globe.</p>



				<p>The Auxilto Group of Companies are supported by a global network of successful entrepreneurs particularly in the Healthcare industry.</p>



				<p>With our connection and know how, the Auxilto Group supports all forms of activities relating to the Healthcare industries, ranging from pharmaceutical technologies, product development, licensing, manufacturing, trading of API and pharmaceutical products, immunological treatment for cancers, merger and acquisition, consultancy services and investment projects etc.</p>



				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis maximus lacus, tincidunt hendrerit orci egestas ac. Fusce malesuada hendrerit laoreet. Praesent blandit sem ut nunc congue suscipit. In sit amet felis non nulla condimentum pretium vel at tortor. Sed laoreet augue nec porta euismod. Aenean ullamcorper leo mauris, eget mattis ipsum sagittis non. Sed et accumsan elit. Vivamus quis ligula nibh.</p>
				</div></div>
			</div>
		</div>
	    <div id="about-us">
			<h3>ABOUT US</h3>
			<h2>The <strong style="color:#01A893;">AUXILTO Value</strong></h2>
			<p>Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?<br><br>

			Auxilto provides a platform with long-standing connections into all industries – from pharmaceuticals to cosmetics, fashion and lifestyle products. We are a global network of successful entrepreneurs, investing our long-term experience, market know-how and creative power into your business success.<br><br></p>

			<p style="color:#01A893; font-weight:bold;">Boost your company with Auxilto!</p>

			<div id="about-info" class="grid-container">
				<div class="grid-item">
				<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/1.png">
				<h5>One Global Network</h5>
				<p>The Auxilto Group of Companies are supported by a global network of successful entrepreneurs particularly in the Healthcare industry</p>
				</div>
				<div class="grid-item">
				<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/2.png">
				<h5>Diversity</h5>
				<p>We respect and value the unique strengths and cultural differences of our associates, customers and community</p>
				</div>
				<div class="grid-item">
				<img src="<?php echo get_url(); ?>wp-content/uploads/2020/06/3.png">
				<h5>Constant improvement</h5>
				<p>We strive for continuous economic optimization and create an added value for your company with our expertise and international network</p>
				</div>
			</div>
		</div>
	</div>

	<?php
}else if( is_page()){
	?>
	<div id="contact-content" class="content">
        <div class="grid-container">
            <div class="grid-item">
                <div class="content" style="text-align: left; padding: 25px;">
                    <h3 style="color: #D65C52; font-size: 20px;">ABOUT US</h3>
                    <h2 style="color: black; text-align: left; font-size: 36px;">The <strong style="color:#01A893; ">AUXILTO Office</strong></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget fermentum leo. Donec vehicula ornare nibh non porttitor. Ut non pharetra sem. Vivamus ac justo nunc. </p>
                </div>
                <img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-1-2.png">
            </div>
            <div class="grid-item">
                <img src="<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-2-1.png">
            </div>
            </div>
        </div>

	<?php
} ?>
