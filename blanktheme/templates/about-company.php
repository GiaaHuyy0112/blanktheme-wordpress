<div id="about-page" class="entry-content">
	<div class="about-us">
		<div style="width: fit-content;">
			<p style="color:#d65c52; font-size: 20px" class="has-text-color"><strong>ABOUT US</strong></p>
			<h2 class="has-very-dark-gray-color has-text-color"><strong>The <span style="color:#01a893" class="has-inline-color">COMPANY</span></strong></h2>
		</div>
		<div class="content" style="margin: auto">
			<p>Auxi Therapeutics Sdn. Bhd., base in Kuala Lumpur, Malaysia, is specialized in CAR-T Cell Therapy against Immune-Related Malignancies. <br><br>

			The availability of such treatment in Malaysia also enable Auxi Therapeutics Sdn. Bhd. in providing similar treatment by cooperating with some leading hospitals in Kuala Lumpur for those patients in the South East Asian Region.
			</p>
		</div>
	</div>
</div>