<div class="about-product">
	<div class="item-left" style="background: url('<?php get_url(); ?>wp-content/uploads/2020/07/Mask-Group-8.png');
										border-radius: 0px 100px 0px 0px; background-repeat: no-repeat;
										background-size: cover;">
		<div class="content" style="text-align: left; width: 50%;float: right; padding: 60px; ">
			<h3>ABOUT US</h3>
			<h2 style="color:black;text-align: left;">The <strong style="color:#01A893">AUXILTO Do</strong></h2>
			<p style="color: white">
			Are you ready to make your start-up marketable? Do you want to open up new possibilities for your company?<br><br>

			Auxilto provides a platform with long-standing connections into all industries – from pharmaceuticals to cosmetics, fashion and lifestyle products. We are a global network of successful entrepreneurs, investing our long-term experience, market know-how and creative power into your business success.<br><br>

			<strong>Boost your company with Auxilto!</strong>
			</p>
		</div>
	</div>
	<div class="item-right" style="width: 100%">
		<div class="product">
			<?php
				$cat = array(
				'post_type' => 'post',
				'category_name' => 'product',
				);
				$loop = new WP_Query($cat);
			 ?>
			 	<div class="grid-container" style="grid-template-columns: auto auto; float: left;">
			 		<?php
			 				while ($loop->have_posts()){
			 					$loop->the_post();
			 					?>
			 						<div class="grid-item box-product" style="border: 1px solid #F0F2F4;
																			box-sizing: border-box;
																			border-radius: 10px;">
										<div class="box-image" style="float: left; padding: 10px;">
			 							<?php the_post_thumbnail( '' ); ?>
			 							</div>
			 							<div class="box-content" >
			 							<h4 style="color: #D65C52; margin: 0"><?php the_title(); ?></h4>
			 							<?php the_content(); ?>
			 							</div>
			 						</div>

			 					<?php
			 				}
			 		 ?>
			 	</div>
		</div>
	</div>
</div>