<div class="popup popup-<?php echo $num ?>" style="width: 50%; height: fit-content; background: white; display: none; position: absolute; z-index: 1;">
	<div class="popup-header" style="grid-template-columns: auto auto; display: grid">
		<div class="images">
			<?php the_post_thumbnail(); ?>
		</div>
		<div class="info">
			<h1><?php the_title(); ?></h1>
			<h3><?php echo $term->name; ?></h3>
		</div>
	</div>
	<div class="popup-content">
		<?php the_content(); ?>
	</div>
</div>