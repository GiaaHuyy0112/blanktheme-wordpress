<?php
  $num = 1;

  function createClass(){
        $terms = get_the_terms(get_the_ID(), 'class');
        if(is_array($terms)){
            foreach( $terms as $term ){
                echo ' product-'.$term->term_id;
            }
        }
  }





?>

<div class="products-list" style="margin-top: 130px; margin-bottom: 50px;">
    <?php
        $companies = get_terms("company");
        foreach ($companies as $company) {
            $array = array();
            wp_reset_query();
             $args = array('post_type' => 'products',
                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => 'company',
                                                    'field' => 'slug',
                                                    'terms' => $company->slug,
                                                ),
                                            ),
                            'order' => 'ASC'
                            );

             $loop = new WP_Query($args);
             if($loop->have_posts()) {?>
                <div class="dropdown-table dropdown-table-<?php echo $num ?>" style="background:#F1F1F1; margin: auto;margin-bottom: 50px; border-radius: 16px;">
                <div class="dropdown-head dropdown-head-<?php echo $num ?>" style="display: flex;background: #F1F1F1;border-radius: 10px; margin: auto;">
                    <h4 class="title" style="font-size: 20px; margin-left: 10px;color: #D65C52;width: 100%;"><?php echo $company->name ?></h4>
                    <h4 class="dropdown-title" 
                        style="font-size: 20px; margin-left: 10px; width: 100%; color: #FFFFFF; display: none">The Product by 
                        <br><strong><?php echo $company->name ?></strong>
                    </h4>
                    <button class="btn-product"  onclick="clickProducts(<?php echo $num; ?>)" style=";align-self: center; background: #01A893; color: white" >
                            View all Products <i class="fa fa-chevron-down" style="font-size: 15px;"></i></button>
                </div>
                <div class="dropdown dropdown-<?php echo $num ?>" style=" display: none;">
                <div class="dropdown-head-content dropdown-head-content-<?php echo $num ?>" style="padding: 20px; text-align: center;">
                    <h3 style="color: #01A893;font-size: 26px; text-align: center; margin: 5px;">PRODUCT BY CATEGORIES</h3>
                    <?php
                        while( $loop->have_posts() ){
                            
                            $loop->the_post();
                            $terms = get_the_terms(get_the_ID(), "class");
                            if(is_array($terms)){
                                foreach( $terms as $term ){
                                    $name = $term->name;
                                    $id =  $term->term_id;
                                    if( !in_array($id, $array) )
                                    {
                                        // array_push($array, $id);
                                        array_push($array, $id);
                                        //array_multisort($array);
                                        //echo $name;
                                    }
                                }
                            }
                        }
                        foreach ($array as $arr) {
                            $termName = get_term_by('id', $arr, 'class');
                                ?>
                                    <button class="category-btn category-btn-<?php echo $arr ?>"
                                            onclick="getClass( <?php echo $num ?> ,<?php echo $arr ?> )" ><?php echo $termName->name; ?></button>
                                <?php
                            }
                    ?>
                </div>

                <?php
                ?> <div class="grid-products grid-products-<?php echo $num ?>" style=" padding: 50px; display: grid;
                                                                                        grid-gap: 15px; width: fit-content; margin: auto;" > <?php
                while($loop->have_posts()) : $loop->the_post();
                    // echo '<a href="'.get_permalink().'">'.get_the_title().'</a><br>';
                    ?>
                        <div class="card products <?php createClass(); ?>" style="background: #FFFFFF; border-radius: 5px;">
                            <?php if ( has_post_thumbnail() ) { ?>
                                <div class="card-image">
                                    <?php the_post_thumbnail( 'thumbnail' ); ?>
                                    <?php } ?>
                                </div>
                            <div class="card-content">
                                    <h4 class="card-title entry-title" style="margin: 0;">
                                    <a class="blog-item-title-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"rel="bookmark" style="color: black">
                                        <?php echo wp_kses( force_balance_tags( get_the_title() ), $allowed_html ); ?>
                                    </a>
                                    </h4>
                                        <p class="card-description"><?php echo wp_kses_post( get_the_excerpt() ); ?></p>
                            </div>
                        </div>
                    <?php
                    // loadProduct();
                endwhile;
                ?></div> </div></div> <?php
             }
             $num++;
        }


    ?>
</div>